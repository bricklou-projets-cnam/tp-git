# Git - Les commandes de bases

Questions: 

1. Deux commits peuvent-ils avoir le même identifiant (hash) ? Si oui, dans quel cas ?

    2 commits ne peuvent pas avoir le même hash, sauf si l'on parle du même commit ou que ces 2 commits ne sont
    pas dans le même dépôt.

2. Peut-on ajouter en une seule commande tous les fichiers du répertoire de travail à l'index ? Si oui, comment ?

    Pour rajouter tout les fichier du répertoire à l'index en une seule commande, il suffit d'exécuter la commande
    suivante:
    ```sh
    git add .
    ```

3. Peut-on ajouter un fichier à l'index si celui-ci n'est pas dans le répertoire de travail ? Si oui, comment ?

    On ne peut pas rajouter un fichier qui se trouve en dehors du répertoire de travail.

4. Pensez-vous que certaines entreprises n'ont pas besoin d'utiliser de VCS ? Justifiez votre réponse en donnant quelques exemples. Ce n'est pas une question piège, la réponse "oui" est admissible tant qu'elle est justifiée.

    L'utilisation de VCS n'est pas une obligation mais plutôt une recommendation dans le cadre de la gestion de projet et de source. Bien qu'il soit possible de travailler
    sereinement sans VCS, il est néanmoins plus intéressant de travailler avec, ne serait-ce que pour versionniser le travail ou même travailler en équipe.

    Bien sûr cela s'applique à des employer qui font de l'informatique. Dans le cadre d'une équipe de support technique, il n'est pas forcément utile d'utiliser un VCS,
    puisqu'ils ne vont pas gérer de projets.

5. Une fois un fichier ajouté à l'index, est-il possible de l'en retirer ? Si oui, par quel moyen ?

    Il est possible de retirer un fichier de l'index via la commande:
    ```sh
    git rm --cached "le_fichier"
    ```

