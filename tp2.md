# Git - Les branches

Questions:

1. La notion de fast-forward permet de ramener les commits d'une branche sur la branche cible. Ces commits seront rajouté à la suite s'il n'y
    à pas de divergence entre les commits.

2. Il y a eu un conflit lors du merge car une ligne a été modifié de chaque côtés, Git n'étant donc pas capable de choisir quel modification
    garder.

3. Après avoir supprimé les branches locales, il est possible de les récupérer à conditions qu'elles aient été envoyé sur le remote.
    Dans le cas contraire, ces branches et leur commits sont perdu.

4. Dans le graphique sur Gitlab, on voit bien l'historique des commits et la fusion des branches avec la divergence.

## Commandes

```sh
git switch -c ajout-style
git add style.css index.html
git commit -m "feat: ajout du fichier style.css"
git checkout main
git switch -c modification-textes
git add index.html
git commit -m "feat: changement du texte"
git checkout main
git merge modification-textes
git merge ajout-styles
git merge --continue
git remote add origin git@gitlab.com:bricklou-projets-cnam/tp-git.git
git push --set-upstream origin main
```